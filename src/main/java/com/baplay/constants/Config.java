package com.baplay.constants;

import java.util.HashMap;

public class Config {
	
	public final static int PAGE_SIZE = 20;
	
	public static final String RETURN_SUCCESS = "0000";
	public static final String RETURN_INPUT_EMPTY = "0001";
	public static final String RETURN_TOKEN_FAIL = "0002";
	public static final String RETURN_DATA_EXIST = "0003";
	public static final String RETURN_MOBILE_FORMATE_ERROR = "0006";
	public static final String RETURN_EMAIL_FORMATE_ERROR = "0007";
	public static final String RETURN_SYSTEM_ERROR = "9999";
	public static final HashMap RETURN_RESULT=new HashMap();
	static{
		RETURN_RESULT.put(RETURN_SUCCESS, "success");
		RETURN_RESULT.put(RETURN_INPUT_EMPTY, "input empty");
		RETURN_RESULT.put(RETURN_TOKEN_FAIL, "token error");
		RETURN_RESULT.put(RETURN_DATA_EXIST, "duplicate data");
		RETURN_RESULT.put(RETURN_MOBILE_FORMATE_ERROR, "mobile format error");
		RETURN_RESULT.put(RETURN_EMAIL_FORMATE_ERROR, "email format error");
		RETURN_RESULT.put(RETURN_SYSTEM_ERROR, "system error");
	}
	
	
}
