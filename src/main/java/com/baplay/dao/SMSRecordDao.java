package com.baplay.dao;

import java.sql.Timestamp;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.baplay.dto.SMSRecord;



/**
 * 簡訊紀錄
 * @version 1.0
 * */
@Repository
public class SMSRecordDao extends BaseDao {
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public SMSRecordDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}

	public SMSRecord findByMessageId(String messageId) {
		SMSRecord smsRecord = (SMSRecord) queryForObject(dsMnMainQry,"SELECT m.* FROM sms_record as m where message_id = ? limit 1",new Object[]{messageId}, SMSRecord.class);
		return smsRecord;
	}
	
	public SMSRecord add(final SMSRecord smsRecord) {
		final String sql = 
				"INSERT INTO sms_record("
				+"message_id"
				+",used_credit"
				+",member_id"
				+",credit"
				+",mobile_no"
				+",status"
				+",create_time"
				+") values "+
				"(?,?,?,?,?,?,?)";
		
		return (SMSRecord) addForObject(dsMnMainUpd, sql, smsRecord, new Object[] { 
				smsRecord.getMessageId(),
				smsRecord.getUsedCredit(),
				smsRecord.getMemberId(),
				smsRecord.getCredit(),
				smsRecord.getMobileNo(),
				smsRecord.getStatus(),
				smsRecord.getCreateTime()
		});
	}
	
	public int updateReport(String messageId,String reportStatus,String sourceProdId,String sourceMessageId, Timestamp reportTime) {
		final String sql = "update sms_record set report_status=?,source_prod_id=?,source_message_id=?,report_time=? where message_id=?";
		return updateForObject(dsMnMainUpd, sql, new Object[] { reportStatus, sourceProdId, sourceMessageId,reportTime, messageId });
	}

}
