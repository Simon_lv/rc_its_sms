package com.baplay.dto;

import java.sql.Timestamp;
/**
 * 簡訊紀錄
 * */
public class SMSRecord extends BaseDto{
	
	private String 		messageId;
	private String 		usedCredit;
	private String 		memberId;
	private String 		credit;
	private String 		mobileNo;
	private String 		status;
	private Timestamp 	createTime;
	private String 		reportStatus;
	private String 		sourceMessageId;
	private Timestamp 	reportTime;
	
	
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getUsedCredit() {
		return usedCredit;
	}
	public void setUsedCredit(String usedCredit) {
		this.usedCredit = usedCredit;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getReportStatus() {
		return reportStatus;
	}
	public void setReportStatus(String reportStatus) {
		this.reportStatus = reportStatus;
	}
	public String getSourceMessageId() {
		return sourceMessageId;
	}
	public void setSourceMessageId(String sourceMessageId) {
		this.sourceMessageId = sourceMessageId;
	}
	public Timestamp getReportTime() {
		return reportTime;
	}
	public void setReportTime(Timestamp reportTime) {
		this.reportTime = reportTime;
	}
	
	
	
}
