package com.baplay.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpClientParams;

public class HttpUtil {
	

	
	public static String sendRequest(String url, String method, NameValuePair[] data)
	{
		HttpClient httpClient = new HttpClient();
		HttpClientParams clientParams = new HttpClientParams();
		clientParams.setContentCharset("UTF-8");
		HttpMethodBase methodBase = null;
		String result = null;
		if("get".equals(method)){
			methodBase = new GetMethod(url);
		}else if("post".equals(method)){
			PostMethod postMethod = new PostMethod(url);
			postMethod.setRequestBody(data);
			methodBase = postMethod;
		}
		try {
			httpClient.executeMethod(methodBase);
			if(methodBase.getStatusCode() == HttpURLConnection.HTTP_OK)
				result = methodBase.getResponseBodyAsString();
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			methodBase.releaseConnection();
		}
		return result;
	}
	  
	public static String sendPost(String url, Map<String, ?> paramMap) {
		StringBuffer param = new StringBuffer();
		for (String key :paramMap.keySet()) {
			param.append('&').append(key).append('=').append(paramMap.get(key));
		}
		return sendPost(url, param.deleteCharAt(0).toString());
	}
	
	 /**
     * 向指定 URL 发送POST方法的请求
     * 
     * @param url
     *            发送请求的 URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
	public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
        	
//        	PrintUtil.outputItem("url", url);
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
//          conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("Content-Type", "application/json");
//          conn.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Transfer-Encoding", "chunked");
            conn.setRequestProperty("Pragma", "no-cache");
            conn.setRequestProperty("Connection", "close");
            conn.setUseCaches(false); 

            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
              
           
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！"+e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        
//        PrintUtil.outputItem("result", result);
        return result;
    }

}
