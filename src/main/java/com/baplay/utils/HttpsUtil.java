package com.baplay.utils;

import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;





public class HttpsUtil {
	private static final RestTemplate REST_TEMPLATE = new RestTemplate();
	
	public static String sendPost(String url, Object request) throws Exception {
		trustAllHttpsCertificates();
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
		ResponseEntity<String> result = REST_TEMPLATE.postForEntity(url, request, String.class);
//		String result = REST_TEMPLATE.postForObject(url, request, String.class, urlVariables);
		return result.getBody();
	}
	
	static HostnameVerifier hv = new HostnameVerifier() {
		public boolean verify(String urlHostName, SSLSession session) {
			System.out.println("Warning: URL Host: " + urlHostName + " vs. "
					+ session.getPeerHost());
			return true;
		}
	};

	private static void trustAllHttpsCertificates() throws Exception {
		javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext
				.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc
				.getSocketFactory());
	}

	static class miTM implements javax.net.ssl.TrustManager,
			javax.net.ssl.X509TrustManager {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public boolean isServerTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public boolean isClientTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public void checkServerTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}

		public void checkClientTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}
	}
	
	public static void main(String[] args) throws Exception {
		String url = "http://113.196.57.113:8133/sendSms.do";
		String pid = "a4cd1da5a0ba4e19bf90dd1fd142b7ee";
		String FRIDAY_DEV_ID = "oHgAy82J3UGhY82KUqBgWQ";
		String FRIDAY_DEV_KEY = "ae4483a7089846d6b120a5bb6a037e31";
		String keyParams = pid+FRIDAY_DEV_ID+FRIDAY_DEV_KEY;
//		PrintUtil.outputItem("keyParams", keyParams);
		String key = SHA256.encrypt(keyParams);
		StringBuilder sb = new StringBuilder();
		sb.append("pid").append("=").append(pid).append("&");
		sb.append("id").append("=").append(FRIDAY_DEV_ID).append("&");
		sb.append("key").append("=").append(key);
		Map<String, Object> urlParameters = new HashMap<String, Object>();

		urlParameters.put("MemberID",		"rcsms");
		urlParameters.put("Password",		"674990c931d1cdcee9f43facf174df6a");
		urlParameters.put("MobileNo",		"0919410129");
		urlParameters.put("CharSet",			"U");
		urlParameters.put("SourceProdID",	"RC");
		urlParameters.put("SourceMsgID",		"RC20150514114504344");
		urlParameters.put("SMSMessage",		"您的RC驗證碼是 10465，有效時間為30分鐘。[RC語音]");
		urlParameters.put("ValidSec",		"7200");
//		String result = sendPost(url, sb.toString().getBytes("UTF-8"), urlParameters);
		String result = sendPost(url, urlParameters);
		System.out.println("result="+result);
	}
}
