/**
 * FpMsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface FpMsService extends javax.xml.rpc.Service {
    public java.lang.String getfpMsServiceSoapAddress();

    public org.tempuri.FpMsServiceSoap getfpMsServiceSoap() throws javax.xml.rpc.ServiceException;

    public org.tempuri.FpMsServiceSoap getfpMsServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
