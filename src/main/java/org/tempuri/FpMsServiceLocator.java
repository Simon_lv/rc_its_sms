/**
 * FpMsServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class FpMsServiceLocator extends org.apache.axis.client.Service implements org.tempuri.FpMsService {

    public FpMsServiceLocator() {
    }


    public FpMsServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FpMsServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for fpMsServiceSoap
    private java.lang.String fpMsServiceSoap_address = "http://ep2.ite2.com.tw/scripts/fpmsservice.asmx";

    public java.lang.String getfpMsServiceSoapAddress() {
        return fpMsServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String fpMsServiceSoapWSDDServiceName = "fpMsServiceSoap";

    public java.lang.String getfpMsServiceSoapWSDDServiceName() {
        return fpMsServiceSoapWSDDServiceName;
    }

    public void setfpMsServiceSoapWSDDServiceName(java.lang.String name) {
        fpMsServiceSoapWSDDServiceName = name;
    }

    public org.tempuri.FpMsServiceSoap getfpMsServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(fpMsServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getfpMsServiceSoap(endpoint);
    }

    public org.tempuri.FpMsServiceSoap getfpMsServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.tempuri.FpMsServiceSoapStub _stub = new org.tempuri.FpMsServiceSoapStub(portAddress, this);
            _stub.setPortName(getfpMsServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setfpMsServiceSoapEndpointAddress(java.lang.String address) {
        fpMsServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.tempuri.FpMsServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                org.tempuri.FpMsServiceSoapStub _stub = new org.tempuri.FpMsServiceSoapStub(new java.net.URL(fpMsServiceSoap_address), this);
                _stub.setPortName(getfpMsServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("fpMsServiceSoap".equals(inputPortName)) {
            return getfpMsServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "fpMsService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "fpMsServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("fpMsServiceSoap".equals(portName)) {
            setfpMsServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
