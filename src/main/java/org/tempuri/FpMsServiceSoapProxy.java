package org.tempuri;

public class FpMsServiceSoapProxy implements org.tempuri.FpMsServiceSoap {
  private String _endpoint = null;
  private org.tempuri.FpMsServiceSoap fpMsServiceSoap = null;
  
  public FpMsServiceSoapProxy() {
    _initFpMsServiceSoapProxy();
  }
  
  public FpMsServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initFpMsServiceSoapProxy();
  }
  
  private void _initFpMsServiceSoapProxy() {
    try {
      fpMsServiceSoap = (new org.tempuri.FpMsServiceLocator()).getfpMsServiceSoap();
      if (fpMsServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)fpMsServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)fpMsServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (fpMsServiceSoap != null)
      ((javax.xml.rpc.Stub)fpMsServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.FpMsServiceSoap getFpMsServiceSoap() {
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap;
  }
  
  public java.lang.String sendSMSwithUserData(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, boolean isLong, java.lang.String data1, java.lang.String data2, java.lang.String data3, java.lang.String data4) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendSMSwithUserData(dstAddr, text, user, pass, scheduleTime, stoptime, isLong, data1, data2, data3, data4);
  }
  
  public java.lang.String sendSMS(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, boolean isLong) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendSMS(dstAddr, text, user, pass, scheduleTime, stoptime, isLong);
  }
  
  public java.lang.String sendDyncSMSwithUserData(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, java.lang.String srcCell, boolean isLong, java.lang.String data1, java.lang.String data2, java.lang.String data3, java.lang.String data4) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendDyncSMSwithUserData(dstAddr, text, user, pass, scheduleTime, stoptime, srcCell, isLong, data1, data2, data3, data4);
  }
  
  public java.lang.String sendDyncSMS(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, java.lang.String srcCell, boolean isLong) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendDyncSMS(dstAddr, text, user, pass, scheduleTime, stoptime, srcCell, isLong);
  }
  
  public java.lang.String sendDyncSMSwithUserDataAndPriority(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, java.lang.String srcCell, boolean isLong, java.lang.String data1, java.lang.String data2, java.lang.String data3, java.lang.String data4, boolean hiPriority) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendDyncSMSwithUserDataAndPriority(dstAddr, text, user, pass, scheduleTime, stoptime, srcCell, isLong, data1, data2, data3, data4, hiPriority);
  }
  
  public java.lang.String sendDyncSMSWithPriority(java.lang.String dstAddr, java.lang.String text, java.lang.String user, java.lang.String pass, java.lang.String scheduleTime, java.lang.String stoptime, java.lang.String srcCell, boolean isLong, boolean hiPriority) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.sendDyncSMSWithPriority(dstAddr, text, user, pass, scheduleTime, stoptime, srcCell, isLong, hiPriority);
  }
  
  public java.lang.String querySMS(java.lang.String user, java.lang.String pass, java.lang.String seq_no) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.querySMS(user, pass, seq_no);
  }
  
  public java.lang.String SMSQueryDr(java.lang.String user, java.lang.String pass, java.lang.String seq_no) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.SMSQueryDr(user, pass, seq_no);
  }
  
  public java.lang.String querySMSWithSendTime(java.lang.String user, java.lang.String pass, java.lang.String seq_no) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.querySMSWithSendTime(user, pass, seq_no);
  }
  
  public java.lang.String querySMSStatusByTimeRange(java.lang.String user, java.lang.String pass, java.lang.String startTime, java.lang.String endTime) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.querySMSStatusByTimeRange(user, pass, startTime, endTime);
  }
  
  public java.lang.String queryReceivedSMS(java.lang.String user, java.lang.String pass, java.lang.String cellId) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.queryReceivedSMS(user, pass, cellId);
  }
  
  public java.lang.String queryReceivedSMSByTimeRange(java.lang.String user, java.lang.String pass, java.lang.String cellId, java.lang.String startTime, java.lang.String endTime) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.queryReceivedSMSByTimeRange(user, pass, cellId, startTime, endTime);
  }
  
  public java.lang.String queryUserPoint(java.lang.String user, java.lang.String pass) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.queryUserPoint(user, pass);
  }
  
  public java.lang.String scheduleSMSCancel(java.lang.String user, java.lang.String pass, java.lang.String seq_no) throws java.rmi.RemoteException{
    if (fpMsServiceSoap == null)
      _initFpMsServiceSoapProxy();
    return fpMsServiceSoap.scheduleSMSCancel(user, pass, seq_no);
  }
  
  
}